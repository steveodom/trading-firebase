'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.setDates = exports.updateDates = exports.updateTickers = exports.saveTicker = exports.update = exports.saveBasics = exports.fetch = exports.fetchTickers = exports.close = undefined;

var _bluebird = require('bluebird');

var _bluebird2 = _interopRequireDefault(_bluebird);

var _firebaseAdmin = require('firebase-admin');

var _firebaseAdmin2 = _interopRequireDefault(_firebaseAdmin);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var initFirebase = function initFirebase(modelId) {
  var serviceAccount = require('./../service-account.json');
  if (_firebaseAdmin2.default.apps.length === 0) {
    _firebaseAdmin2.default.initializeApp({
      credential: _firebaseAdmin2.default.credential.cert(serviceAccount),
      databaseURL: 'https://trading-predictions.firebaseio.com'
    });
  };
  var db = _firebaseAdmin2.default.database();
  db.goOnline();
  var ref = db.ref(modelId);
  return {
    tickersRef: ref.child('tickers'),
    datesRef: ref.child('dates'),
    ref: ref
  };
};

var close = exports.close = function close() {
  if (_firebaseAdmin2.default.apps.length > 0) {
    return _firebaseAdmin2.default.database().goOffline();
  }
};

var fetchTickers = exports.fetchTickers = function fetchTickers(modelId) {
  var _initFirebase = initFirebase(modelId),
      tickersRef = _initFirebase.tickersRef;

  return tickersRef.once('value').then(function (snapshot) {
    return snapshot.val();
  }, function (err) {
    console.log("The read failed: " + err.code);
    return _bluebird2.default.reject(err.code);
  });
};

var fetch = exports.fetch = function fetch(modelId, child) {
  var _initFirebase2 = initFirebase(modelId),
      ref = _initFirebase2.ref;

  return ref.child(child).once('value').then(function (snapshot) {
    return snapshot.val();
  }, function (err) {
    console.log("The read failed: " + err.code);
    return _bluebird2.default.reject(err.code);
  });
};

var saveBasics = exports.saveBasics = function saveBasics(modelId, tickers, changeCalc) {
  var _initFirebase3 = initFirebase(modelId),
      ref = _initFirebase3.ref;

  var basics = {
    changeCalc: changeCalc,
    members: tickers
  };
  return ref.update(basics).then(function (res) {
    return true;
  });
};

var update = exports.update = function update(modelId, data) {
  var _initFirebase4 = initFirebase(modelId),
      ref = _initFirebase4.ref;

  return ref.update(data).then(function (res) {
    return true;
  }, function (err) {
    console.log("The update failed: " + err.code);
    return _bluebird2.default.reject(err.code);
  });
};

var saveTicker = exports.saveTicker = function saveTicker(modelId, ticker, dateStr, result) {
  var _initFirebase5 = initFirebase(modelId),
      tickersRef = _initFirebase5.tickersRef;

  var dateRef = tickersRef.child(ticker).child(dateStr);
  return dateRef.set(result).then(function (res) {
    return true;
  });
};

var updateTickers = exports.updateTickers = function updateTickers(modelId, result) {
  var _initFirebase6 = initFirebase(modelId),
      tickersRef = _initFirebase6.tickersRef;

  return tickersRef.update(result).then(function (res) {
    return true;
  }, function (err) {
    console.log("The read failed: " + err.code);
    return _bluebird2.default.reject(err.code);
  });
};

var updateDates = exports.updateDates = function updateDates(modelId, result) {
  var _initFirebase7 = initFirebase(modelId),
      datesRef = _initFirebase7.datesRef;

  return datesRef.update(result).then(function (res) {
    return true;
  });
};

var setDates = exports.setDates = function setDates(modelId, result) {
  var _initFirebase8 = initFirebase(modelId),
      datesRef = _initFirebase8.datesRef;

  return datesRef.set(result).then(function (res) {
    return true;
  });
};