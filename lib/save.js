'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.closeFirebase = exports.fbUpdate = exports.fetchByChild = exports.fetchTickerArray = exports.saveModelInfo = exports.savePredictions = undefined;

var _keys = require('babel-runtime/core-js/object/keys');

var _keys2 = _interopRequireDefault(_keys);

var _bluebird = require('bluebird');

var _bluebird2 = _interopRequireDefault(_bluebird);

var _formatter = require('./formatter');

var _firebase = require('./firebase');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var savePredictions = exports.savePredictions = function savePredictions(modelId, results, date) {
  var formatted = (0, _formatter.predictionResults)(results, date);
  if (!formatted || !formatted.tickers || !formatted.dates) {
    _bluebird2.default.reject('data not structured propertly');
  }

  var dates = (0, _firebase.updateDates)(modelId, formatted.dates);
  return dates.then(function (res) {
    return _bluebird2.default.resolve(res);
  }).catch(function (err) {
    console.info('err?', err);
    _bluebird2.default.reject('error saving', modelId);
  });
};

var saveModelInfo = exports.saveModelInfo = function saveModelInfo(modelId, tickers, changeCalc) {
  return (0, _firebase.saveBasics)(modelId, tickers, changeCalc).catch(function (err) {
    console.info('err?');
  });
};

// returns the whole ticker object. Used to get a raw list of tickers to write to a flatten 'member' list
var fetchTickerArray = exports.fetchTickerArray = function fetchTickerArray(modelId) {
  return (0, _firebase.fetchTickers)(modelId).then(function (raw) {
    return _bluebird2.default.resolve((0, _keys2.default)(raw));
  });
};

// members is a flat list of arrays
var fetchByChild = exports.fetchByChild = function fetchByChild(modelId, child) {
  return (0, _firebase.fetch)(modelId, child).then(function (raw) {
    return _bluebird2.default.resolve(raw);
  });
};

var fbUpdate = exports.fbUpdate = function fbUpdate(modelId, data) {
  return (0, _firebase.update)(modelId, data).catch(function (err) {
    console.info('error', err);
    return err;
  });
};

var closeFirebase = exports.closeFirebase = function closeFirebase() {
  (0, _firebase.close)();
};