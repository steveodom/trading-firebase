'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.buildMemberArray = exports.predictionResults = undefined;

var _keys = require('babel-runtime/core-js/object/keys');

var _keys2 = _interopRequireDefault(_keys);

var _lodash = require('lodash');

var _lodash2 = _interopRequireDefault(_lodash);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var predictionResults = exports.predictionResults = function predictionResults(results, date) {
  var formatted = {
    tickers: {},
    dates: {}
  };

  results.forEach(function (obj) {
    if (_lodash2.default.isEmpty(obj.ticker)) {
      obj.ticker = 'noTicker';
    }
    (0, _keys2.default)(obj).forEach(function (key) {
      if (key !== 'ticker') {
        var tickerKey = obj.ticker + '/' + date + '/' + key;
        var dateKey = date + '/' + obj.ticker + '/' + key;

        var val = obj[key];
        if (_lodash2.default.isNaN(val)) {
          val = 'empty';
        }

        formatted.tickers[tickerKey] = val;
        formatted.dates[dateKey] = val;
      }
    });
  });
  return formatted;
};

var buildMemberArray = exports.buildMemberArray = function buildMemberArray(tickers) {
  return true;
};