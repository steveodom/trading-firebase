Helpers for storing predictions in firebase.

https://console.firebase.google.com/project/trading-predictions/database/

To update live data, use src/utils/fb/clearRef method. Use it like:

node -e 'require("./lib/utils/fb").clearRef("ml-daffy-siskin", {"tickers/": null})'

If you make any changes to utils/fb.js, be sure to run npm run build prior to running it.