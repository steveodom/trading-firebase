import _ from 'lodash';

export const predictionResults = (results, date) => {
  const formatted = {
    tickers: {},
    dates: {}
  }

  results.forEach( (obj) => {
    if (_.isEmpty(obj.ticker)) {
      obj.ticker = 'noTicker';
    }
    Object.keys(obj).forEach( (key) => {
      if (key !== 'ticker') {
        const tickerKey = `${obj.ticker}/${date}/${key}`;
        const dateKey = `${date}/${obj.ticker}/${key}`;

        let val = obj[key];
        if (_.isNaN(val)) {
          val = 'empty';
        }

        formatted.tickers[tickerKey] = val;
        formatted.dates[dateKey] = val;
      }
    });
    
  });
  return formatted
}

export const buildMemberArray = (tickers) => {
  return true; 
}