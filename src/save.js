import Promise from 'bluebird';
import {predictionResults} from './formatter';
import {updateDates, close, saveBasics, fetchTickers, fetch, update} from './firebase';

export const savePredictions = (modelId, results, date) => {
  const formatted = predictionResults(results, date);
  if (!formatted || !formatted.tickers || !formatted.dates) {
    Promise.reject('data not structured propertly');
  }
  
  const dates = updateDates(modelId, formatted.dates);
  return dates.then( (res) => {
    return Promise.resolve(res);
  }).catch( (err) => {
    console.info('err?', err);
    Promise.reject('error saving', modelId);
  })
}

export const saveModelInfo = (modelId, tickers, recipe) => {  
  return saveBasics(modelId, tickers, recipe).catch( (err) => {
    console.info('err?')
  });
}

// returns the whole ticker object. Used to get a raw list of tickers to write to a flatten 'member' list
export const fetchTickerArray = (modelId) => {
  return fetchTickers(modelId).then( (raw) => {
    return Promise.resolve(Object.keys(raw));
  });
}

// members is a flat list of arrays
export const fetchByChild = (modelId, child) => {
  return fetch(modelId, child).then( (raw) => {
    return Promise.resolve(raw);
  });
}

export const fbUpdate = (modelId, data) => {
  return update(modelId, data).catch( (err) => {
    console.info('error', err);
    return err;
  });
}


export const closeFirebase = () => {
  close();
}