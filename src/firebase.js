import Promise from 'bluebird';
import firebase from 'firebase-admin';

const initFirebase = (modelId) => {
  const serviceAccount = require('./../service-account.json');
  if (firebase.apps.length === 0) {
    firebase.initializeApp({
      credential: firebase.credential.cert(serviceAccount),
      databaseURL: 'https://trading-predictions.firebaseio.com'
    });
  };
  const db = firebase.database();
  db.goOnline();
  const ref = db.ref(modelId);
  return {
    tickersRef: ref.child('tickers'), 
    datesRef: ref.child('dates'), 
    ref
  }
}

export const close = () => {
  if (firebase.apps.length > 0) {
    return firebase.app().delete();
  }
}

export const fetchTickers = (modelId) => {
  const {tickersRef} = initFirebase(modelId);
  return tickersRef.once('value').then( (snapshot) => {
    return snapshot.val();
  }, (err) => {
    console.log("The read failed: " + err.code);
    return Promise.reject(err.code);
  });
}

export const fetch = (modelId, child) => {
  const {ref} = initFirebase(modelId);
  return ref.child(child).once('value').then( (snapshot) => {
    return snapshot.val();
  }, (err) => {
    console.log("The read failed: " + err.code);
    return Promise.reject(err.code);
  });
}

export const saveBasics = (modelId, tickers, recipe) => {
  const {ref} = initFirebase(modelId);
  const basics = {
    recipe: recipe,
    changeCalc: recipe.changeCalc,
    members: tickers
  }
  return ref.update(basics).then( (res) => {
    return true;
  });
}

export const update = (modelId, data) => {
  const {ref} = initFirebase(modelId);
  return ref.update(data).then( (res) => {
    return true;
  }, (err) => {
    console.log("The update failed: " + err.code);
    return Promise.reject(err.code);
  });
}

export const saveTicker = (modelId, ticker, dateStr, result) => {
  const {tickersRef} = initFirebase(modelId);
  const dateRef = tickersRef.child(ticker).child(dateStr);
  return dateRef.set(result).then( (res) => {
    return true;
  });
}

export const updateTickers = (modelId, result) => {
  const {tickersRef} = initFirebase(modelId);
  return tickersRef.update(result).then( (res) => {
    return true;
  }, (err) => {
    console.log("The read failed: " + err.code);
    return Promise.reject(err.code);
  });
}

export const updateDates = (modelId, result) => {
  const {datesRef} = initFirebase(modelId);
  return datesRef.update(result).then( (res) => {
    return true;
  });
}

export const setDates = (modelId, result) => {
  const {datesRef} = initFirebase(modelId);
  return datesRef.set(result).then( (res) => {
    return true;
  });
}
