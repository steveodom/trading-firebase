import Promise from 'bluebird';
import {update, close} from './../firebase';

export const clearRef = (model, data) =>  {
  return update(model, data).then( (res) => {
    close();
    return res;
  })
};
