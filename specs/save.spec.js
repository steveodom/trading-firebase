import {expect} from 'chai';
import { savePredictions, fetchTickerArray, fetchByChild } from './../src/save';
import Promise from 'bluebird';

describe('Save - savePredictions', () => {
  it('should save results correctly', () => {
    const prediction = [
      {ticker: 'aa', score: 0.5 },
      {ticker: 'bb', score: 0.4} 
    ];
    const modelId = 'testing-birds';
    const date = '12062016';
    return savePredictions(modelId, prediction, date).then(( res) => {
      return expect(typeof res).to.equal('boolean');

    });
  });

  it('should returns an array of true/false results of whether each was saved succesffuly', () => {
    const prediction = [
      {ticker: 'aa', score: 0.5 },
      {ticker: 'bb', score: 0.4} 
    ];
    const modelId = 'testing-birds';
    const date = '12062016';
    return savePredictions(modelId, prediction, date).then(( res) => {
      return expect(res).to.equal(true);

    });
  });

  it('should handle human realtime date ', () => {
    const prediction = [
      {ticker: 'aa', score: 0.5 },
      {ticker: 'bb', score: 0.4} 
    ];
    const modelId = 'testing-birds';
    const date = 'daily-01-09-2017-03:35';
    return savePredictions(modelId, prediction, date).then(( res) => {
      return expect(res).to.equal(true);

    });
  });
});

describe('Save - fetchTickerArray', () => {
  it('should return an array of tickers', () => {
    const modelId = 'testing-birds';
    return fetchTickerArray(modelId).then(( res) => {
      expect(Array.isArray(res)).to.equal(true);
      return expect(res.includes('aa')).to.equal(true);
    });
  });
});