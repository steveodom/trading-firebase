import {expect} from 'chai';
import { predictionResults } from './../src/formatter';
import Promise from 'bluebird';

describe('Formatter - predictionResults', () => {
  it('should return an object with ticker and dates properties', () => {
    const prediction = [
      {ticker: 'aapl', score: 0.5 },
      {ticker: 'twa', score: 0.4} 
    ]

    const res = predictionResults(prediction, '12062016');
    expect(typeof res).to.equal('object');
    expect(res).to.have.property('tickers');
    expect(res).to.have.property('dates');
  });

  it('should hydrate the tickers property with an object containing the score', () => {
    const prediction = [
      {ticker: 'aapl', score: 0.5 },
      {ticker: 'twa', score: 0.4} 
    ]
    const date = '12062016';
    const formatted = predictionResults(prediction, date);
    const res = formatted.tickers;
    expect(res).to.have.property(`aapl/${date}/score`);
    expect(res).to.have.property(`twa/${date}/score`);

    expect(res[`aapl/${date}/score`]).to.equal(0.5);
    expect(res[`twa/${date}/score`]).to.equal(0.4);
  });

  it('should hydrate the date property with an object containing the score', () => {
    const prediction = [
      {ticker: 'aapl', score: 0.5 },
      {ticker: 'twa', score: 0.4} 
    ]
    const date = '12062016';
    const formatted = predictionResults(prediction, date);
    const res = formatted.dates;
    expect(res).to.have.property(`${date}/aapl/score`);
    expect(res[`${date}/aapl/score`]).to.equal(0.5);
  });

  it('should remove NaN values', () => {
    const prediction = [
      {ticker: '', score: NaN } 
    ]
    const date = '12062016';
    const formatted = predictionResults(prediction, date);
    const res = formatted.dates;
    expect(res).to.have.property(`${date}/noTicker/score`);
    expect(res[`${date}/noTicker/score`]).to.equal('empty');
  });
});