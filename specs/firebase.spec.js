import {expect} from 'chai';
import { fetchTickers, saveTicker, updateTickers, updateDates, setDates, saveBasics, fetch, close, update } from './../src/firebase';
import Promise from 'bluebird';

describe('Firebase - fetchTickers', () => {
  it('should return an object with tickers as keys', (done) => {
    const modelId = 'testing-birds';
    fetchTickers(modelId)
      .then( (res) => {
        console.info('how quick', res)
        expect(typeof res).to.equal('object');
        expect(res).to.have.property('yes');
        done();
      });
    
  });
});

describe('Firebase - fetch', () => {
  it('should return the results of the child object', (done) => {
    const modelId = 'testing-birds';
    fetch(modelId, 'changeCalc')
      .then( (res) => {
        expect(typeof res).to.equal('object');
        expect(res).to.have.property('yes');
        done();
      });
    
  });
});

describe('Firebase - saveTicker', () => {
  it('should save the result passed in', (done) => {
    const modelId = 'testing-birds';
    const result = { score: 0.6, result: 5};

    saveTicker(modelId, 'aapl', '12062016', result)
      .then( (res) => {
        expect(res).to.equal(true);
        done();
      });
  });
});

describe('Firebase - updateTickers', () => {
  it('should save the results passed in', (done) => {
    const modelId = 'testing-birds';
    const result = {
      'aapl/120716-3': { score: 0.6, result: 5 },
      'yes/120716-3': { score: 0.7, result: 6 }
    };

    updateTickers(modelId, result)
      .then( (res) => {
        expect(res).to.equal(true);
        done();
      });
  });

  it('should save results without erasing previous things', (done) => {
    const modelId = 'testing-birds';
    const result = {
      'aapl/120716-3b': { score: 0.6, result: 5 }
    };

    updateTickers(modelId, result)
      .then( (res) => {
        expect(res).to.equal(true);
        done();
      });
  });

  it('should save additional properties', (done) => {
    const modelId = 'testing-birds';
    const result = {
      'aapl/120716-3b/added': 'yes'
    };

    updateTickers(modelId, result)
      .then( (res) => {
        expect(res).to.equal(true);
        done();
      });
  });

  it('should save an example lambda one', (done) => {
    const modelId = 'testing-birds';
    const result = {
      'aapl/12212016': { close: '0.1' }
    };

    updateTickers(modelId, result)
      .then( (res) => {
        expect(res).to.equal(true);
        done();
      });
  });
});

describe('Firebase - updateDates', () => {
  it('should save the results passed in', (done) => {
    const modelId = 'testing-birds';
    const result = {
      '120716-3/aapl/result': 5,
      '120716-3/yes/result': -2
    };

    updateDates(modelId, result)
      .then( (res) => {
        expect(res).to.equal(true);
        done();
      });
  });

  it('should be able to add a property to each using set', (done) => {
    const modelId = 'testing-birds';
    const result = {
      '120716-3/aapl/score': 0.6,
      '120716-3/yes/score': 0.5
    };

    updateDates(modelId, result)
      .then( (res) => {
        expect(res).to.equal(true);
        done();
      });
  });
});

describe('Firebase - saveBasics', () => {
  it('should save the results passed in', (done) => {
    const modelId = 'testing-birds';
    const changeCalc = {
      method: 'isLessThan',
      n: 2,
      threshold1: -1,
      threshold2: -0.5
    };
    const recipe = {
      changeCalc,
      tags: ['test']
    }
    const tickers = ['aa', 'bb', 'cc'];

    saveBasics(modelId, tickers, recipe)
      .then( (res) => {
        expect(res).to.equal(true);
        done();
      });
  });
});


describe('Firebase - update', () => {
  it('should save the results passed in', (done) => {
    const modelId = 'queue';

    update(modelId, {'simmering-bumpkins': null})
      .then( (res) => {
        expect(res).to.equal(true);
        done();
      });
  });

  it('should save the results passed in', (done) => {
    const modelId = 'models';

    update(modelId, {'daffy-siskin': {tags: ['active', 'daily']}})
      .then( (res) => {
        expect(res).to.equal(true);
        done();
      });
  });

  // it('should remove entire child by setting values this way', (done) => {
  //   const modelId = 'queue';

  //   update(modelId, {'testing-birds': {tests: null}})
  //     .then( (res) => {
  //       expect(res).to.equal(true);
  //       done();
  //     });
  // });
  
  it('should return an error when trying to remove child but not other siblings when set this way', (done) => {
    const modelId = 'queue/testing-birds/test';
    update(modelId, null)
      .then( (res) => {
        // this should not be hit
        console.info('success?')
        expect(res).to.equal(false);
        done();
      }, (err) => {
        console.info(err, 'are we here?');
        expect(true).to.equal(true);
        done();
      });
  });

  it('should remove child but not other siblings when set this way', (done) => {
    const modelId = 'queue/testing-birds';
    update(modelId, {test: null})
      .then( (res) => {
        // this should not be hit
        console.info('success?')
        expect(res).to.equal(false);
        done();
      }, (err) => {
        console.info(err, 'are we here?');
        expect(true).to.equal(true);
        done();
      });
  });
});

